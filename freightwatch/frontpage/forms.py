#!/usr/bin/env python3

from django import forms
from django.core.exceptions import ValidationError
import datetime
import json

import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)

import bahn

"""
solution from: 
https://stackoverflow.com/questions/714063/importing-modules-from-parent-folder?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
"""

class City(forms.Form):
    def form(self):
        return forms.CharField(
            label="Stadt", 
            max_length=200, 
            validators=self.check_city()
            )

    def check_city(self):
        r = self.bahn.geonames(place=self.form(), local=False)
        rd = {}
        if r is not None:
            if r["totalResultsCount"] > 0:
                if r["totalResultCount"] > 1:
                    for place in r["geonames"]:
                        
                        b = bahn.DataImport().betriebsstelle(
                            name=place["name"], 
                            local=False)
            else:
                raise ValidationError(
                    "Ort %(ort)s nicht gefunden", 
                    params={"ort":self.form}
                    )
        else:
            print("Verbindungsfehler")


class DateField(forms.Form):
    
    def form(self, name=None, helptext=None):
        def datefield(self):
            d = forms.DateField(
                initial=datetime.date.today(), 
                label=name, 
                help_text=helptext)
            return d

    def check_dates(self):
        if self.datefield > datetime.date.today():
            raise ValidationError('Datum liegt in der Zukunft')
        return data
