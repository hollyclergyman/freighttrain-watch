#!/usr/bin/env python3 

"""
script to process data input from APIs and insert data into database
"""
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)

import bahn


import sys

"""
solution from: 
https://stackoverflow.com/questions/714063/importing-modules-from-parent-folder?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
"""
sys.path.append(os.path.join(os.path.dirname(__file__), "freightwatch"))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "freightwatch.settings")

import django
"""
These steps have to be called, in order to include third party scripts 
for interacting with the models.
Solutions come from: 
-   http://blog.gabrielsaldana.org/using-django-models-in-external-python-scripts/
    for the import, the final settings import does not seem 
    necessary
-   https://github.com/Koed00/django-q/issues/216
    answer by Eagllus, explaining django.setup() call ahead of 
    processing
"""
django.setup()

from frontpage.models import Stadt, Strecke, Bahnhof
from bahn.requests import DataImport
from xml.etree.ElementTree import ElementTree as ET

class DataProcessing():
    def __init__(self):
        self.di = DataImport()

    """
    always execute manage.py migrate when 
    changing anything about the models
    """

    def stadt_import(self):
        l = ["village", "town", "city"]
        for v in l:
            idata = self.di.places_from_overpass(place=v)
            if idata is not None:
                for place in idata:
                    if "lat" in place and "lon" in place:
                        lat = place["lat"]
                        lon = place["lon"]
                        tags = place["tags"]
                        if "name" in tags:
                            name = place["tags"]["name"]
                            if "is_in" in tags:
                                is_in = place["tags"]["is_in"].split(",")
                                b_land = is_in[-3] 
                            if Stadt.objects.filter(longitude__contains=lon) and Stadt.objects.filter(latitude__contains=lat):
                                continue
                            else:
                                if "is_in" in tags:
                                    r = Stadt(
                                        ort=name, 
                                        latitude=lat, 
                                        longitude=lon, 
                                        bundesland=b_land
                                    )
                                else:
                                    r = Stadt(
                                        ort=name,
                                        latitude=lat,
                                        longitude=lon
                                    )
                                r.save()
                                #print(name)

    def strecken_import(self):
        _in = self.di.railways_from_overpass()
        d = {}
        for line in _in:
            if 'ref' in line['tags']:
                streda = line['tags']['ref']
                if 'nodes' in line:
                    if streda in d:
                        for v in line['nodes']:
                            d[streda].append(v)
                        else:
                            d[streda] = line['nodes']


        
