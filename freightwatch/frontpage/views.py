#!/usr/bin/env python3


from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from .models import Bahnhof, Stadt, Strecke
from .forms import City, DateField


def bahnhof(request):
    b = Bahnhof(bahnhof="Stuttgart Hbf")
    
    return HttpResponse(b)
    # Don't forget to render view responses as HttpResponse!!!!!

def next(request):
    if request.method == 'POST':
        stadt = City(request.POST)
        bd = DateField.check_dates(
            name="Startdatum", 
            helptext="Bitte geben Sie ein Startdatum für die Abfrage ein")
        ed = DateField.check_dates(
            name="Enddatum", 
            helptext="Bitte geben Sie ein Enddatum für die Abfrage ein")
    return HttpResponse(bd, ed)

# Create your views here.
