#!/usr/bin/env python3


from django.db import models
from django.contrib.gis.db import models as gismodels
from django import forms


# Create your models here.
class Bahnhof(models.Model):
    bahnhof = models.CharField(
        max_length=100, 
        help_text="Bitte tragen Sie einen Bahnhof ein, für den Sie suchen wollen")
    _id = models.IntegerField()
    stadt = models.ForeignKey("Stadt", on_delete=False, null=False)
    strecken = models.CharField(
        max_length=200, 
        help_text="Strecken, an denen der Bahnhof liegt")
    longitude = gismodels.FloatField()
    latitude = gismodels.FloatField()
    nbahnhoefe = models.CharField(
        max_length=1000, 
        help_text="Die nächsten Bahnhöfe, getrennt nach Strecke")
    distbahnhoefe = models.CharField(
        max_length=1000, 
        help_text="Distanz bis zum nächsten Bahnhof")
    """
    Bahnhof → station, defined by: 
    _id → internal id from German Railways and 
    stadt → place where station is located
    longitude and latitude → important for 
    the geographical position of the station

    to calculate, where the train is travelling 
    and how long this will take, 
    the neighbouring stations and their distances are either saved
    format for geographical position:
    WGS84 in decimal structure
    """
    
    class Meta:
        """
        this is an internal class of django, which sets the metadata of the models object
        """
        ordering = ["bahnhof"]
        """
        sets how the elements are aligned when returned, 
        in this case, the ordering follows the station names
        """
    
    def get_absolute_url(self):
        return reverse("model-detail-view", args=[str(self.id)])

    def __str__(self):
        return self.bahnhof

class Stadt(models.Model):
    ort = models.CharField(
        max_length=100, 
        help_text="Bitte tragen Sie einen Ort für die Abfrage ein")
    longitude = gismodels.FloatField()
    latitude = gismodels.FloatField()


    class Meta:
        ordering = ["ort"]

    def get_absolute_url(self):
        return reverse("model-detail-view", args=[str(self.id)])

    def __str__(self):
        return self.ort

class Strecke(models.Model):
    strecke = models.CharField(
        max_length=200, 
        help_text="Streckenname oder Name der Orte an der Strecke")
    bahnhoefe = models.CharField(
        max_length=1000, 
        help_text="Alle Bahnhofs-IDs die an der Strecke liegen")
    active = models.BooleanField()
    # this field is to be set, for each line to ensure only 
    # operational stations are considered
    electrified = models.BooleanField()
    max_velocity = models.IntegerField()

    _id = models.IntegerField()
    """
    the STREDA id by German railways is taken for 
    the identification of each line
    """
    class Meta:
        ordering = ["strecke"]

    def get_absolute_url(self):
        return reverse("model-detail-view", args=[str(self.id)])

    def __str__(self):
        return self.strecke

