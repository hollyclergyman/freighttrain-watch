#!/usr/bin/env python3

from django.db import models
import datetime

# Create your models here.
class License(models.Model):
    title = models.CharField(
        max_length=200
    )
    datum = models.DateField()
    version = models.CharField(
        max_length=100
    )
    lizenz = models.CharField(
        max_length=12000
    )

    class Meta:
        ordering = ["title"]

    def get_absolute_url(self):
        return reverse("model-detail-view", args=[str(self.text)])

    def __str__(self):
        return self.product