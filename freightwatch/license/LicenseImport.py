#!/usr/bin/env python3

from .models import License

class LicenseImport():
    def clean_html(self):
       with open("LICENSE.html", "r", encoding="utf-8") as lfile:
            l = []
            for line in lfile:
                l.append(line)
            removal_lines = {}
            n = 0
            for rl in l:
                if '<!--' in rl:
                    n = n + 1
                    removal_lines[n] = [l.index(rl)]
                if '-->' in rl:
                    removal_lines[n].append(l.index(rl))
            for key, values in removal_lines.items():
                if key % 2 == 1:
                    rlv = values[1] + 1
                    if key + 1 in removal_lines:
                        nv = removal_lines[key + 1]
                        nrlv = nv[0]
                        l = l[rlv:nrlv]
                    else:
                        break
            final = []
            for line in l:
                if line.startswith('\n'):
                    final.append(line.replace('\n', '<br>'))
                else:
                    utf = line.replace("\'", "'")
                    final.append(utf.strip('\n'))
            return final

    def import_license(self, from_html=False, ititle=None, iversion=None, idate=None, ilicense=None):
        if from_html is True:
            ifile = self.clean_html()
            ititle = ifile[0].lstrip()
            iversion = ifile[1].split(',')[0].lstrip()
            idate = ifile[1].split(',')[1].strip()
            ilicense = ifile[2:]
            t = ""
            for l in ilicense:
                t = t + l
        if License.objects.filter(title__contains=title):
            continue
        else:
            l = License(
                title=ititle, 
                version=iversion, 
                date=idate, 
                lizenz=ilicense
            )
            