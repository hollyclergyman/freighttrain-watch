#!/usr/bin/env python3

from .requests import DataImport

class __init__():

    def betriebsstelle(name):
        return DataImport().betriebsstelle(name)

    def zug_betrstelle_by_name(name):
        return DataImport().zug_betrstelle_by_name(name)
        
    def requests(url, parameter, auth=None):
        return DataImport().request_api(url, parameter, auth)