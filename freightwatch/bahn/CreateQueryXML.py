#!/usr/bin/env python3

import xml.etree.ElementTree as ET

class CreateQueryXML():
    def create_stations_xml(self, name):#, place, request_aim):
        """
        https://wiki.openstreetmap.org/wiki/Overpass_API/Language_Guide#The_languages
        https://pymotw.com/2/xml/etree/ElementTree/create.html
        """
        root = ET.Element("osm-script")
        root.set("timeout", "900")
        root.set("output", "json")
        
        union1 = ET.SubElement(root, "union")
        union1.set("into", "a")
        query1 = ET.SubElement(union1, "query")
        query1.set("type", "area")
        has_kv1 = ET.SubElement(query1, "has-kv")
        has_kv1.set("k", "ISO3166-1")
        has_kv1.set("v", "DE")
        has_kv1_1 = ET.SubElement(query1, "has-kv")
        has_kv1_1.set("k", "admin_level")
        has_kv1_1.set("v", "2")

        union2 = ET.SubElement(root, "union")
        union2.set("into", "b")
        query2 = ET.SubElement(union2, "query")
        query2.set("into", "_")
        query2.set("type", "area")
        has_kv2 = ET.SubElement(query2, "has-kv")
        has_kv2.set("k", "name")
        has_kv2.set("v", name)
        area_query1 = ET.SubElement(query2, "area-query")
        area_query1.set("from", "a")
        area_query1.set("into", "_")
        area_query1.set("ref", "")

        union3 = ET.SubElement(root, "union")
        union3.set("into", "_")
        query3 = ET.SubElement(union3, "query")
        query3.set("type", "node")
        has_kv3 = ET.SubElement(query3, "has-kv")
        has_kv3.set("k", "railway")
        has_kv3.set("v", "station")
        area_query2 = ET.SubElement(query3, "area-query")
        area_query2.set("from", "b")
        area_query2.set("into", "_")
        area_query2.set("ref", "")

        query4 = ET.SubElement(union3, "query")
        query4.set("into", "_")
        query4.set("type", "node")
        area_query3 = ET.SubElement(query4, "area-query")
        area_query3.set("from", "b")
        area_query3.set("into", "_")
        area_query3.set("ref", "")
        has_kv4 = ET.SubElement(query4, "has-kv")
        has_kv4.set("k", "railway")
        has_kv4.set("v", "halt")

        """
        xml according to the following converter link:
        http://www.overpass-api.de/api/convert?data=%3Cosm-script+output%3D%22json%22+timeout%3D%22900%22%3E%3Cunion+into%3D%22a%22%3E%3Cquery+type%3D%22area%22%3E%3Chas-kv+k%3D%22ISO3166-1%22+v%3D%22DE%22+%2F%3E%3Chas-kv+k%3D%22admin_level%22+v%3D%222%22+%2F%3E%3C%2Fquery%3E%3C%2Funion%3E%3Cunion+into%3D%22b%22%3E%3Cquery+into%3D%22_%22+type%3D%22area%22%3E%3Chas-kv+k%3D%22name%22+v%3D%22Stuttgart%22+%2F%3E%3Carea-query+from%3D%22a%22+into%3D%22_%22+ref%3D%22%22+%2F%3E%3C%2Fquery%3E%3C%2Funion%3E%3Cunion+into%3D%22_%22%3E%3Cquery+type%3D%22node%22%3E%3Chas-kv+k%3D%22railway%22+v%3D%22station%22+%2F%3E%3Carea-query+from%3D%22b%22+into%3D%22_%22+ref%3D%22%22+%2F%3E%3C%2Fquery%3E%3Cquery+into%3D%22_%22+type%3D%22node%22%3E%3Carea-query+from%3D%22b%22+into%3D%22_%22+ref%3D%22%22+%2F%3E%3Chas-kv+k%3D%22railway%22+v%3D%22halt%22+%2F%3E%3C%2Fquery%3E%3C%2Funion%3E%3Cprint+from%3D%22_%22+%2F%3E%3C%2Fosm-script%3E&target=mapql
        """

        pr = ET.SubElement(root, "print")
        pr.set("from", "_")

        return ET.tostring(root)

    def create_places_xml(self, place):
        root = ET.Element("osm-script")
        root.set("output", "json")
        root.set("timeout", "900")
        
        union1 = ET.SubElement(root, "union")
        union1.set("into", "a")
        query1 = ET.SubElement(union1, "query")
        query1.set("type", "area")
        has_kv1 = ET.SubElement(query1, "has-kv")
        has_kv1.set("k", "ISO3166-1")
        has_kv1.set("v", "DE")
        has_kv1_1 = ET.SubElement(query1, "has-kv")
        has_kv1_1.set("k", "admin_level")
        has_kv1_1.set("v", "2")

        union2 = ET.SubElement(root, "union")
        union2.set("into", "_")
        query2 = ET.SubElement(union2, "query")
        query2.set("type", "node")
        has_kv2 = ET.SubElement(query2, "has-kv")
        has_kv2.set("k", "place")
        has_kv2.set("v", place)
        area_query = ET.SubElement(query2, "area-query")
        area_query.set("from", "a")
        area_query.set("into", "_")
        area_query.set("ref", "")

        pr = ET.SubElement(root, "print")
        pr.set("from", "_")

        """
        xml according to the following converter link:
        http://www.overpass-api.de/api/convert?data=%5Bout%3Ajson%5D%0D%0A%5Btimeout%3A900%5D%3B%0D%0A%28area%5B%22ISO3166-1%22%3D%22DE%22%5D%5B%22admin_level%22%3D%222%22%5D%3B%29-%3E.a%3B%0D%0A%28node%5B%22place%22%3D%22town%22%5D%28area.a%29%3B%29%3B%0D%0Aout%3B&target=xml
        """
        return ET.tostring(root)

    def railways_xml(self):
        root = ET.Element('osm-script')
        root.set("output","json")
        root.set("timeout","900")
        union1 = ET.SubElement(root, "union")
        union1.set("into", "a")
        query1 = ET.SubElement(union1, "query")
        query1.set("type", "area")
        has_kv1 = ET.SubElement(query1, "has-kv")
        has_kv1.set("k", "ISO3166-1")
        has_kv1.set("v", "DE")
        has_kv1_1 = ET.SubElement(query1, "has-kv")
        has_kv1_1.set("k", "admin_level")
        has_kv1_1.set("v", "2")
        union2 = ET.SubElement(root, "union")
        query2 = ET.SubElement(union2, "query")
        query2.set("type", "way")
        has_kv2 = ET.SubElement(query2, "has-kv")
        has_kv2.set("k", "railway")
        has_kv2.set("v", "rail")
        area = ET.SubElement(query2, "area-query")
        area.set("from", "a")
        area.set("into", "_")
        area.set("ref", "")
        pr = ET.SubElement(root, "print")
        pr.set("from", "_")

        return ET.tostring(root)

"""
<osm-script output="json" output-config="" timeout="900">
  <union into="_">
    <query into="_" type="area">
      <has-kv k="ISO-3166-1" modv="" v="DE"/>
      <has-kv k="admin_level" modv="" v="2"/>
    </query>
  </union>
  <query into="_" type="-&gt;">
    <item set="a"/>
  </query>
  <union into="_">
    <query into="_" type="way">
      <has-kv k="railway" modv="" v="rail"/>
      <has-kv k="type" modv="" v="route"/>
      <area-query from="a" into="_" ref=""/>
    </query>
  </union>
  <print e="" from="_" geometry="skeleton" limit="" mode="body" n="" order="id" s="" w=""/>
</osm-script>
"""