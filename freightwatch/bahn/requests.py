#!/usr/bin/env python3

"""
script to download data from various open APIs
"""

import certifi  # necessary for the certificate validation in the connection process
import urllib3  # necessary to access the API as a web interface
import glob
import json
import csv
import numpy as np
import pandas as pd
from .CreateQueryXML import CreateQueryXML


class DataImport():
    def __init__(self):
        self.createqueryxml = CreateQueryXML()
        self.overpass_api = "https://lz4.overpass-api.de/api/interpreter"
        self.http = urllib3.PoolManager(
            cert_reqs='CERT_REQUIRED',
            ca_certs=certifi.where())

    def request_api(self, url, parameter, auth=None, body=False):
        try:
            if auth is not None:
                urlrequest = self.http.request(
                    "GET",
                    url,
                    parameter,
                    headers=auth)
            elif auth is None and body is True:
                urlrequest = self.http.request(
                    "GET",
                    url,
                    body=parameter
                    # body has to be set when dealing with 
                    # xml files in REST queries
                )
            elif auth is None and body is False:
                urlrequest = self.http.request(
                    "GET", 
                    url, 
                    parameter)
                    # auth is None means, there is no authentication header, 
                    # authentication happens in the submitted data
            if urlrequest.status != 200:
                print("Fehler beim Abfragen der API", urlrequest.status)
                return None
            res = json.loads(urlrequest.data.decode("utf-8"))
            urlrequest.release_conn()
            return res
        except urllib3.exceptions.HTTPError:
            print("Netzwerkfehler beim Abrufen der Informationen")
            return None
        except urllib3.exceptions.ConnectTimeoutError:
            print("Connection Timeout")
            return None
        except urllib3.exceptions.MaxRetryError:
            print("Maximale Anzahl an Versuchen ausgeschöpft")
            return None

    def betriebsstelle(self, name, local=False):
        api = "https://api.deutschebahn.com/betriebsstellen/v1/betriebsstellen"
        parameters = {"name":name}
        auth_token = self.auth_file(
            service="bahn", 
            local=local, 
            bearer=True)
        r = self.request_api(
            url=api, 
            parameter=parameters, 
            auth=auth_token)
        if r is not None:
            rd = {}
            for element in r:
                if "status" in element:
                    if "in use" in element["status"]:
                        betrname = element["name"]
                        strbtrname = betrname.replace("  ", "")
                        '''
                        removes multiple whitespaces in string
                        https://stackoverflow.com/questions/8270092/remove-all-whitespace-in-a-string-in-python
                        '''
                        if element["id"] is not None:
                            print(element)
                            rd[strbtrname] = [element["id"], element["abbrev"]]
            return rd
    
    def auth_file(self, service, local=False, bearer=False):
        if local is False:
            path = glob.glob("../bahn/credentials.json")[0]
            """
            the glob.glob function returns a list, from which the first element is taken
            solution from: 
            https://stackoverflow.com/questions/2186525/use-a-glob-to-find-files-recursively-in-python/6484486?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
            """
        elif local is True:
            path = glob.glob("credentials.json")[0]
            """
            this condition enables the function to run from 
            functions in the same directory
            """
        with open(path, "r", encoding="utf-8") as credentials:
            creds = json.load(credentials)
            if bearer is True:
                btoken = creds[service]["application_token"]
                return {'Authorization': 'Bearer {}'.format(btoken)}
            else:
                if service in creds:
                    return creds[service]

    def zug_betrstelle(self, start, to, name=None, _id=None, lon=None, lat=None, local=False):
        if name is not None and _id is None and lat is None and lon is None:
            api = "https://api.deutschebahn.com/cargo/v1/delays"
            parameters = {
                "name":name,
                "from":start,
                "to":to
                }
        elif _id is not None and name is None and lat is None and lon is None:
            api = "https://api.deutschebahn.com/cargo/v1/delays/" + str(_id)
            parameters = {
                "from":start,
                "to":to
            }
        elif lat is not None and lon is not None and name is None and _id is None:
            api = "https://api.deutschebahn.com/cargo/v1/delays/loc/{}/{}".format(lat, lon)
            parameters = {
                "from":start,
                "to":to
            }
        else:
            print("Fehler in den eingegebenen Daten")
            return None
        auth_token = self.auth_file(
            service="bahn", 
            local=local, 
            bearer=True
        )
        r = self.request_api(
            url=api, 
            parameter=parameters, 
            auth=auth_token
        )
        if r is not None:
            return r

    def data_from_nominatim(self, long=None, lat=None, local=False):
        """
        longitude and latitude are parameters in 
        WGS84 floating point style
        """
        if long is None and lat is None:
            return None
        if long is None or lat is None:
            return None
        path = self.auth_file(
            service="openstreetmap", 
            local=local)
        with open(path, "r", encoding="utf-8") as credentials:
            creds = json.load(credentials)
            uname = creds["username"]
            api = "https://nominatim.openstreetmap.org/reverse"
            parameter = {
                "format":"json",
                "lat":lat, 
                "long":long,
                "email":uname
            }
            r = self.request_api(url=api, parameter=parameter)
            return r

    def stations_from_overpass(self, name): #, request_aim
        """
        All data are extracted from the overpass API, which provides 
        read only access at openstreetmap data
        """
        #if "station" in request_aim:
        #    ad = {
        #        
        #    }
        api_file = self.createqueryxml.create_stations_xml(name)
        #req = '[timeout:900];(area["ISO3166-1"="DE"][admin_level=2];)->.a;(node.a[amenity="station"];way.a[amenity="station"];);out;'
        #req = req.encode(encoding="ascii")
        # conversion to ASCII is necessary to avoid errors from the api
        r = self.request_api(
            url=self.overpass_api, 
            parameter=api_file, 
            body=True
        )
        if r is not None:
            return r["elements"]

    def places_from_overpass(self, place):
        places_xml = self.createqueryxml.create_places_xml(place=place)
        r = self.request_api(
            url=self.overpass_api, 
            parameter=places_xml, 
            body=True)
        if r is not None:
            return r["elements"]

    def railways_from_overpass(self):
        railways_xml = self.createqueryxml.railways_xml()
        r = self.request_api(
            url=self.overpass_api,
            parameter=railways_xml,
            body=True
        )
        if r is not None:
            return r["elements"]